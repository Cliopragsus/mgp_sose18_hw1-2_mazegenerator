﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RandomMazeGenerator : MonoBehaviour {


	[SerializeField] GameObject m_mazeElement_A;
	[SerializeField] GameObject m_mazeElement_B;
    public GameObject[] elements;

	[SerializeField] int m_mazeWidth;
	[SerializeField] int m_mazeHeight;

	/// <summary>
	/// The distance between the maze elements.
	/// </summary>
	[SerializeField] float m_distanceMazeElements;

	/// <summary>
	/// Generate a Maze on Startup
	/// </summary>
	void Start () {

		GenerateMaze ();
	}
		
	/// <summary>
	/// Place randomly the two different maze elements
	/// </summary>
	void GenerateMaze()
	{
		Vector3 elementPosition = Vector3.zero  ;
		GameObject elementObject = null;
		int counter_width = 0; 
		int counter_height = 0; 

		///itterate through two loops to the height and width of the maze 
		while(counter_width < m_mazeWidth) 
		{
			while (counter_height < m_mazeHeight) 
			{
				///calculate the position of the element from the center
				elementPosition.x = (counter_width - (m_mazeWidth / 2)) * m_distanceMazeElements;
				elementPosition.z = (counter_height - (m_mazeHeight / 2)) * m_distanceMazeElements;

                /*
				///randomly instantiate the one or the other element
				if (Random.value <= 0.5f) {
					elementObject = Instantiate (m_mazeElement_A, elementPosition, m_mazeElement_A.transform.rotation);
					elementObject.name = "MazeElement_A_";
				} else {
					elementObject = Instantiate (m_mazeElement_B, elementPosition, m_mazeElement_B.transform.rotation);
					elementObject.name = "MazeElement_B_";
				}
                */
                GameObject newElement = elements[Random.Range(0, 5)];

                GameObject currentElement = Instantiate(newElement, elementPosition, newElement.transform.rotation);

				currentElement.name += counter_width + "_" + counter_height;

				counter_height++;
			}

			counter_height = 0;
			counter_width++;

		}
	}
}
